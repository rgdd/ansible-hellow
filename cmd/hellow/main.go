package main

import (
	"flag"
	"fmt"
	"log"
	"net/http"
)

func main() {
	optAddr := flag.String("addr", "localhost:8080", "HTTP server address in the format ADDR:PORT")
	flag.Parse()

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		log.Printf("Received request from: %s\n", r.RemoteAddr)
		fmt.Fprintf(w, "Hello world\n")
		if err := r.Body.Close(); err != nil {
			log.Fatalf("Error closing request body: %v", err)
		}
	})

	log.Printf("Starting server at %s", *optAddr)
	if err := http.ListenAndServe(*optAddr, nil); err != nil {
		log.Fatalf("Error starting server: %v", err)
	}
}
